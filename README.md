### What is this repository for? ###

This plugin provides a basic keypad for general use. (I got tired of needing a generic keypad and not being able to find a simple one, so I just built it.)

### How do I get set up? ###

Control Pins:  
    String: Outputs the string that shows in the input box after numbers are pushed.  
    Clear Trigger/Enter Trigger: Sends a Trigger signal whenever the button is pushed.
	
Properties:  
    Enter Clears Box: Select whether or not the input box is cleared when enter is pushed.

### Notes ###

This keypad is meant to be a versitile piece of the bigger picture.

Currently, it only submits the input, but in the future I'd like to add optional functionality, such as a built-in access/pin system. Please let me know if there's any functionality that would be useful to you.


### Who do I talk to? ###

Please feel free to contact me through my profile on Comunities. You can find me [here](https://developers.qsc.com/s/profile/0054X00000DxT9HQAV).